//
//  ViewController.swift
//  trashItemATURL_iCloud_Bug
//
//  Created by Рома on 6/24/16.
//  Copyright © 2016 RomanTikhonychev. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    //MARK: Properties
    
    
    @IBOutlet weak var logTextField: NSTextField!
    
    @IBOutlet weak var openFileButton: NSButton!
    @IBOutlet weak var trashItemAtURLButton: NSButton!
    
    dynamic var logString: String = "Please open file"
    
    var fileToTrashURL : NSURL? = nil

    //MARK: Lifecycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    //MARK: Actions
    
    
    @IBAction func openButtonDidPushed(sender: AnyObject) {
     print("openButtonDidPushed")
        
       //print(self.logString)
        //self.openFile()
        self.listDirectory()
    }

    @IBAction func trashItemAtURLButtonPushed(sender: AnyObject) {
     print("trashItemAtURLButtonPushed")
        
        if self.fileToTrashURL == nil {
            let alert : NSAlert = NSAlert.init()
            alert.informativeText = "Select file for deleting"
            alert.messageText = "Select file first"
            alert.alertStyle = NSAlertStyle.WarningAlertStyle
            alert.runModal()
        } else {
            
            let fileManager : NSFileManager = NSFileManager.defaultManager()
            if !fileManager.isUbiquitousItemAtURL(self.fileToTrashURL!){ //!
                
                let alert : NSAlert = NSAlert.init()
                alert.informativeText = "Select file which situated in Desctop or Document directory (must be synced with iCloud)"
                alert.messageText = "Not iCloud sync file"
                alert.alertStyle = NSAlertStyle.WarningAlertStyle
                alert.runModal()
                
            } else {
                
                do {
                    var resultingURL : NSURL? = nil
                    try fileManager.trashItemAtURL(self.fileToTrashURL!, resultingItemURL: &resultingURL)
                    
                    self.logString = self.logString + " trashItemAtURL had been invoken: \(resultingURL?.path)\n"
                    
                    putBackItemAtURL(resultingURL!, oldURL: self.fileToTrashURL!)
                } catch let error {
                    print(error)
                    self.logString = self.logString + "ERROR: " + String(error) + "\n"
                }
                
            }
            
        }
        
        
        
    }
    
    func putBackItemAtURL(url: NSURL, oldURL: NSURL) {
        do {
            try  NSFileManager.defaultManager().moveItemAtURL(url, toURL: oldURL)
            self.logString = self.logString + "PUT BACK!"
        }
        catch {
            self.logString = self.logString + "put back FAIL"
        }
        
    }
    
    //MARK: Functions
    
    
    func listDirectory() {
        
        let myOpenDialog: NSOpenPanel = NSOpenPanel()
        myOpenDialog.canChooseDirectories = true
        myOpenDialog.runModal()
        
        let path = myOpenDialog.URL?.path
        print(myOpenDialog.URL)
        self.fileToTrashURL = myOpenDialog.URL
        self.trashItemAtURLButton.enabled = true
        
        if (path != nil) {
            
         self.logString = "Selected file is: \(path!)\n"
        }
    }
}

